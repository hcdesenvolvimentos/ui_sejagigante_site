$(function(){
      
    //CARROSSEL DE DESTAQUE
    $("#carrosselDestaque").owlCarousel({
        items : 1,
        dots: true,
        loop: false,
        lazyLoad: true,
        mouseDrag:true,
        touchDrag  : true,         
        autoplayTimeout:5000,
        autoplayHoverPause:true,
        smartSpeed: 450,

        //CARROSSEL RESPONSIVO
        //responsiveClass:true,             
        /*responsive:{
            320:{
                items:1
            },
            600:{
                items:2
            },
           
            991:{
                items:2
            },
            1024:{
                items:3
            },
            1440:{
                items:4
            },
                                    
        }*/                             
        
    });


    var userFeed = new Instafeed({
       get: 'user',
       userId: '245759082',
       clientId: 'a1545960a7d345029a42367a9a2c0a4a',
       accessToken: '245759082.1677ed0.971992e7e1be49efadcb3f4d635dae03',
       resolution: 'standard_resolution',
       template: '<a href="{{link}}" target="_blank" id="{{id}}"><div class="itemInstagram" style="background:url({{image}})"><small class="likeComments"><span class="likes">{{likes}}</span><span class="comments">{{comments}}</span></small></div></a>',
       sortBy: 'most-recent',
       limit: 8,
       links: false
     });
     userFeed.run();

     // SCRIPTS HEIGHT 100% MODAL
    $(window).bind('scroll', function () {
       var alturaScroll = $(window).scrollTop()
       if (alturaScroll > 50) {
            $("header").addClass("topoFixed");
       }else{
            $("header").removeClass("topoFixed");
       }
    });

     $('a.scrollTop').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 1000);
                return false;
            }
        }
        
    });

    $('button.minha-ferramenta').click(function(){
        $('.pg-popup').slideDown();
    });

    $('.fechar').click(function(){
        $('.pg-popup').slideUp();
    });
});
